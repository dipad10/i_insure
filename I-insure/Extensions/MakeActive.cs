﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPolicy.Extensions
{
    public static class MakeActive
    {
     

        public static string MakeActiveController(this UrlHelper urlHelper, string controller)
        {
            string result = "active active-page";

            string controllerName = urlHelper.RequestContext.RouteData.Values["controller"].ToString();

            if (!controllerName.Equals(controller, StringComparison.OrdinalIgnoreCase))
            {
                result = null;
            }

            return result;
        }


    }
}