//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class MktGroupUser
    {
        public long MktGroupUserID { get; set; }
        public string MktGroupID { get; set; }
        public string UserID { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
