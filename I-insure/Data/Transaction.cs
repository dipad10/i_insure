//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction
    {
        public long TransactionID { get; set; }
        public string VoucherNo { get; set; }
        public Nullable<System.DateTime> TDate { get; set; }
        public string TDescription { get; set; }
        public string Trans_Type { get; set; }
        public string PartnerID { get; set; }
        public string PartnerName { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> ApprovedAmt { get; set; }
        public Nullable<decimal> ChequeAmt { get; set; }
        public Nullable<decimal> A1 { get; set; }
        public Nullable<decimal> A2 { get; set; }
        public Nullable<decimal> A3 { get; set; }
        public Nullable<decimal> A4 { get; set; }
        public Nullable<decimal> A5 { get; set; }
        public Nullable<byte> Deleted { get; set; }
        public Nullable<byte> Active { get; set; }
        public string D1 { get; set; }
        public string D2 { get; set; }
        public string D3 { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        public string ChequeNo { get; set; }
        public string AcctNo { get; set; }
        public string AcctName { get; set; }
        public string OptionType { get; set; }
        public string Remarks { get; set; }
        public string Tag { get; set; }
        public string TransGUID { get; set; }
        public Nullable<int> status { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> ModiffiedOn { get; set; }
        public string ModiffiedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> PrintedOn { get; set; }
        public string PrintedBy { get; set; }
    }
}
