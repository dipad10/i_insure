//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SecUser
    {
        public long SN { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string permission { get; set; }
        public Nullable<int> PermissionID { get; set; }
    }
}
