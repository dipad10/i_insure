//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SubRisk
    {
        public string SubRiskID { get; set; }
        public string RiskID { get; set; }
        public string MidRiskID { get; set; }
        public string MidRisk { get; set; }
        public string SubRisk1 { get; set; }
        public string Description { get; set; }
        public Nullable<byte> Deleted { get; set; }
        public Nullable<byte> Active { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
