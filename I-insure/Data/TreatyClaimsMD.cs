//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TreatyClaimsMD
    {
        public int TreatyClmID { get; set; }
        public string ClaimNo { get; set; }
        public string DNCNNO { get; set; }
        public string BrCode { get; set; }
        public string PolicyNo { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public Nullable<double> Amount { get; set; }
        public string LossDetails { get; set; }
        public Nullable<System.DateTime> TransDate { get; set; }
        public Nullable<System.DateTime> NotifyDate { get; set; }
        public Nullable<System.DateTime> LossDate { get; set; }
        public string RiskCode { get; set; }
        public string InsuredName { get; set; }
        public string RecType { get; set; }
        public Nullable<decimal> FacAmt { get; set; }
        public Nullable<double> CessionRate { get; set; }
        public Nullable<decimal> CessionAmt { get; set; }
        public Nullable<double> Lossb4Rate { get; set; }
        public Nullable<decimal> Lossb4Amt { get; set; }
        public Nullable<double> Surp1Rate { get; set; }
        public Nullable<decimal> Surp1Amt { get; set; }
        public Nullable<double> Surp2Rate { get; set; }
        public Nullable<decimal> Surp2Amt { get; set; }
        public Nullable<decimal> ExcessLossAmt { get; set; }
        public Nullable<decimal> NetLoss { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public Nullable<System.DateTime> ExtraDate1 { get; set; }
        public Nullable<System.DateTime> ExtraDate2 { get; set; }
        public Nullable<decimal> A1 { get; set; }
        public Nullable<decimal> A2 { get; set; }
        public Nullable<decimal> A3 { get; set; }
        public Nullable<decimal> A4 { get; set; }
        public Nullable<decimal> A5 { get; set; }
        public string SubmitBy { get; set; }
        public Nullable<System.DateTime> SubmitOn { get; set; }
        public string ApproveBy { get; set; }
        public Nullable<System.DateTime> ApproveOn { get; set; }
        public string TransGUID { get; set; }
        public string TransSTATUS { get; set; }
        public string Tag { get; set; }
    }
}
