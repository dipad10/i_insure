//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class InsuredClient
    {
        public string InsuredID { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string OtherNames { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string MobilePhone { get; set; }
        public string LandPhone { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Remarks { get; set; }
        public string InsuredType { get; set; }
        public string Profile { get; set; }
        public string Contperson { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<byte> Deleted { get; set; }
        public Nullable<byte> Active { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public Nullable<decimal> A1 { get; set; }
        public Nullable<decimal> A2 { get; set; }
        public Nullable<decimal> A3 { get; set; }
        public Nullable<decimal> A4 { get; set; }
        public Nullable<decimal> A5 { get; set; }
        public Nullable<System.DateTime> ExtraDate1 { get; set; }
        public Nullable<System.DateTime> ExtraDate2 { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string MeansID { get; set; }
        public string MeansIDNo { get; set; }
        public string AgentID { get; set; }
        public string MktStaffID { get; set; }
        public string passportno { get; set; }
        public string gender { get; set; }
        public string alternative_email { get; set; }
        public string tin { get; set; }
        public bool TopRisk { get; set; }
    }
}
