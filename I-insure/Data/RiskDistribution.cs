//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class RiskDistribution
    {
        public long DistributionID { get; set; }
        public string SubRiskID { get; set; }
        public string UnderYr { get; set; }
        public Nullable<double> CessionRate { get; set; }
        public double Limit { get; set; }
        public decimal QoutaShare_Commission { get; set; }
        public decimal QoutaShare_profitcommission { get; set; }
        public decimal QoutaShare_ManagementExpenses { get; set; }
        public decimal QoutaShare_PFLossWithdrawal { get; set; }
        public decimal QoutaShare_PFPremiumWithdrawal { get; set; }
        public Nullable<decimal> Retention { get; set; }
        public Nullable<decimal> Retention_Additional { get; set; }
        public Nullable<decimal> Surplus1 { get; set; }
        public decimal Surplus1_SlidingScaleCommission { get; set; }
        public decimal Surplus1_ProfitCommission { get; set; }
        public decimal Surplus1_ManagementExpenses { get; set; }
        public decimal Surplus1_PFLossWithdrawal { get; set; }
        public decimal Surplus1_PFPremiumWithdrawal { get; set; }
        public decimal Surplus1_LossParticipation { get; set; }
        public Nullable<decimal> Surplus2 { get; set; }
        public Nullable<double> LineNos { get; set; }
        public Nullable<decimal> TreatyCapacity { get; set; }
        public string Transguid { get; set; }
        public string Tag { get; set; }
        public string SubmitBy { get; set; }
        public Nullable<System.DateTime> SubmitOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public double LineNos2 { get; set; }
        public double LineNos3 { get; set; }
        public decimal Surplus3 { get; set; }
        public double LineNos4 { get; set; }
        public decimal Surplus4 { get; set; }
    }
}
