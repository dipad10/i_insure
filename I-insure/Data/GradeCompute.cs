//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace I_insure.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class GradeCompute
    {
        public long SN { get; set; }
        public string RegID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Othernames { get; set; }
        public string AcademicSession { get; set; }
        public string Term { get; set; }
        public string Class { get; set; }
        public string Subject { get; set; }
        public Nullable<long> CA1 { get; set; }
        public Nullable<long> CA2 { get; set; }
        public Nullable<long> CA3 { get; set; }
        public Nullable<long> CA4 { get; set; }
        public Nullable<long> CATotal { get; set; }
        public Nullable<long> ExamScore { get; set; }
        public Nullable<long> TotalExamScore { get; set; }
        public string Comment { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public string TransGUID { get; set; }
        public string Status { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string Tag { get; set; }
    }
}
